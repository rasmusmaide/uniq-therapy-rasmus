<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Exception;
use DB;
use App\Patient;
use Log;
use Carbon\Carbon;

class HomeController extends Controller
{
    //
    public function index()
    {
        return view('frontend/index');
    }

    public function contact()
    {
        return view('frontend/contact');
    }

    public function getRegister(Request $request)
    {
        try {
            
            if($request['email'] == null)
            {
                return back()->with('error', 'There is some error, please try again.');     
            }
            $validatedData = $request->validate([
                'name' => 'required',
                'email' => 'required',
            ]);
            
            if($validatedData)
            {
                $input = $request->all();
                $user_info = [
                    'full_name' => $input['name'],
                    'email' =>($input['email']),
                    'gender' =>'',
                    'country' => '',
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ];
                
                $data = DB::table('patients')->insert($user_info);
                
                return back()->with('success', 'Thanks for your informartion.');     
            
            } else {
                
                return back()->with('error', 'There is some error, please try again.');     
            
            }
            

        } catch (Exception $e) {
            
            Log::error(
                'Register via email method exception (getRegister()):' . PHP_EOL .
                'File: ' . $e->getFile() . PHP_EOL .
                'Line: ' . $e->getLine() . PHP_EOL .
                $e->getMessage() . PHP_EOL . PHP_EOL . $e->getTraceAsString()
            );
            
            return view('frontend/contact');
        }
    }
}