@include('layout.header')
<header style="margin-top:30px">
    
    </header>
  <div class="container">
    <div class="row">

        <div class="col-md-5 mb-5 border-light p-5" style="margin-right:2px">
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris non bibendum libero,
         id tincidunt quam. Nam ut nunc metus. Suspendisse potenti. Curabitur dui dui,
          facilisis sed pulvinar eget, dignissim eget ex. Proin id dapibus nulla.
           Pellentesque viverra pulvinar nisi, id consectetur lacus malesuada in. 
           Suspendisse sit amet turpis euismod, gravida quam eget, vehicula arcu. 
           Fusce facilisis enim at ipsum cursus luctus. Cras ut convallis velit.
            Nullam at justo rhoncus, rutrum orci ut, interdum mauris. 
        </div>
        <div class="col-md-6 mb-5 border border-light p-5">
        
        
        @if ($message = Session::get('success'))
          <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>	
                  <strong>{{ $message }}</strong>
          </div>
        @endif


          @if ($message = Session::get('error'))
          <div class="alert alert-danger alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>	
                  <strong>{{ $message }}</strong>
          </div>
          @endif
        <h4>Please register yourself</h4>
        <form method="post"  enctype="multipart/form-data">
        {{ csrf_field() }}
          <div class="form-group">
            <label for="name">Full Name</label>
            <input type="text" class="form-control" id="name" name="name" placeholder="Full name">
          </div>
          <div class="form-group">
            <label for="email" style="text-align:left">Email address</label>
            <input type="email" class="form-control" id="email" name="email" aria-describedby="emailHelp" placeholder="Enter email">
          </div>
          
<!--           
          <div class="form-group">
            <label for="exampleInputFile">File input</label>
            <input type="file" class="form-control-file" id="exampleInputFile" aria-describedby="fileHelp">
            <small id="fileHelp" class="form-text text-muted">This is some placeholder block-level help text for the above input. It's a bit lighter and easily wraps to a new line.</small>
          </div> -->
          
          
          <button type="submit" class="btn btn-primary">Register</button>
        </form>
        </div>
        
      </div>
    </div>  
    <!-- /.row -->
  

  </div>
  <!-- /.container -->

  @include('layout.footer')
