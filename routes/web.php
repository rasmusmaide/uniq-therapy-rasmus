<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', 'HomeController@index');
Route::post('/contact', 'HomeController@getRegister');
Route::get('/contact', 'HomeController@contact');

Route::get('/refresh-auth-token', 'Api\UserController@refreshAuthToken');